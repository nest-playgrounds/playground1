import { Injectable, Logger, NestMiddleware } from '@nestjs/common';

@Injectable()
export class TasksMiddleware implements NestMiddleware {
  private readonly logger = new Logger(TasksMiddleware.name);

  use(req: any, res: any, next: () => void) {
    this.logger.log('here');
    next();
  }
}
