import { Process, Processor } from '@nestjs/bull';
import { Job } from 'bull';

@Processor('audio')
export class AudioConsumer {
  @Process('tasks')
  async transcode(job: Job<unknown>) {
    let progress = 0;
    console.log('receive' + ++progress);
    console.log(job.data);
    await job.progress(100);
    return {};
  }
}
