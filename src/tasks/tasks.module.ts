import {
  CacheModule,
  MiddlewareConsumer,
  Module,
  NestModule,
} from '@nestjs/common';
import { TasksController } from './tasks.controller';
import { TasksService } from './services/tasks.service';
import { TasksMiddleware } from './middlewares/tasks-middleware.service';
import { BullModule } from '@nestjs/bull';
import { AudioConsumer } from './consumers/audio-consumer.class';
import * as redisStore from 'cache-manager-redis-store';

@Module({
  controllers: [TasksController],
  providers: [TasksService, AudioConsumer],
  imports: [
    BullModule.registerQueue({
      name: 'audio',
    }),
    CacheModule.register({
      store: redisStore,
      host: 'localhost',
      port: 6379,
    }),
  ],
})
export class TasksModule implements NestModule {
  configure(consumer: MiddlewareConsumer): any {
    consumer.apply(TasksMiddleware).forRoutes(TasksController);
  }
}
