import { CACHE_MANAGER, Inject, Injectable, Logger } from '@nestjs/common';
import { Observable, of } from 'rxjs';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';
import { Cron, CronExpression } from '@nestjs/schedule';
import { OnEvent } from '@nestjs/event-emitter';
import { Cache } from 'cache-manager';

@Injectable()
export class TasksService {
  private readonly logger = new Logger(TasksService.name);
  constructor(
    @InjectQueue('audio') private audioQueue: Queue,
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
  ) {}

  public async getAll(): Promise<Observable<string>> {
    const job = await this.audioQueue.add(
      'tasks',
      {
        id: 1,
        name: 'New Task',
      },
      { delay: 3000 },
    );
    return of('Hello Tasks');
  }

  @Cron(CronExpression.EVERY_30_SECONDS)
  handleCron() {
    this.logger.debug('Called EVERY_30_SECONDS');
  }

  @OnEvent('order')
  handleOrderCreatedEvent(payload: { id }) {
    console.log(payload);
  }

  async checkCache() {
    return await this.cacheManager.get('key');
  }

  async setCache() {
    await this.cacheManager.set('key', 'val');
    return 'ok';
  }
}
