import { Controller, Get } from '@nestjs/common';
import { TasksService } from './services/tasks.service';
import { EventEmitter2 } from '@nestjs/event-emitter';

@Controller('tasks')
export class TasksController {
  constructor(
    private tasksService: TasksService,
    private eventEmitter: EventEmitter2,
  ) {}

  @Get()
  public async getAll() {
    return this.tasksService.getAll();
  }

  @Get('event')
  public emit() {
    this.eventEmitter.emit('order', { id: 2 });
  }

  @Get('setCache')
  public async setCache() {
    return this.tasksService.setCache();
  }

  @Get('checkCache')
  public async checkCache() {
    return await this.tasksService.checkCache();
  }
}
